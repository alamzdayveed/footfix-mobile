package me.codegod.footfix;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import me.codegod.footfix.adapter.CompetitionsViewPagerAdapter;
import me.codegod.footfix.fragments.main.CompetitionsFragment;

public class CompetitionActivity extends AppCompatActivity {
    public static final String TAG = CompetitionActivity.class.getSimpleName();
    private ViewPager competitionsViewPager;
    private CompetitionsViewPagerAdapter competitionsViewPagerAdapter;
    private TabLayout tabLayout;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_competition);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();

        competitionsViewPager = (ViewPager) findViewById(R.id.pager);
        competitionsViewPagerAdapter = new CompetitionsViewPagerAdapter(getSupportFragmentManager());
        competitionsViewPager.setAdapter(competitionsViewPagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tab);
        tabLayout.setupWithViewPager(competitionsViewPager);

        Intent intent = getIntent();

        setTitle(intent.getStringExtra(CompetitionsFragment.COMPETITION_NAME));
        editor.putInt(CompetitionsFragment.COMPETITION_ID, intent.getIntExtra(CompetitionsFragment.COMPETITION_ID, 0));
        editor.apply();
    }
}
