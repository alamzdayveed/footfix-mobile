package me.codegod.footfix;

import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import me.codegod.footfix.fragments.main.CompetitionsFragment;
import me.codegod.footfix.fragments.main.TodayFixturesFragment;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.bottom_action_fixtures:
                    getSupportActionBar().setTitle("Today's Fixtures");
                    loadTodayFixturesFragment();
                    return true;
                case R.id.bottom_action_competitions:
                    getSupportActionBar().setTitle("Competitions");
                    loadCompetitions();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if (savedInstanceState == null) {
            getSupportActionBar().setTitle("Today's Fixtures");
            loadTodayFixturesFragment();
        }
    }

    private void loadTodayFixturesFragment() {
        TodayFixturesFragment fragment = TodayFixturesFragment.newInstance();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame, fragment);
        ft.commit();
    }

    private void loadCompetitions() {
        CompetitionsFragment fragment = CompetitionsFragment.newInstance();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame, fragment);
        ft.commit();
    }
}
