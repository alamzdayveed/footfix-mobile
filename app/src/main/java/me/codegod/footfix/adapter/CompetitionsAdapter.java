package me.codegod.footfix.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import me.codegod.footfix.R;
import me.codegod.footfix.model.competitions.Competition;

public class CompetitionsAdapter extends RecyclerView.Adapter<CompetitionsAdapter.CompetitionViewHolder> {
    private List<Competition> competitions;
    private Context context;

    private OnItemClickListener listener;

    public CompetitionsAdapter(List<Competition> competitions, Context context) {
        this.competitions = competitions;
        this.context = context;
    }

    @NonNull
    @Override
    public CompetitionViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.competition_item, viewGroup, false);

        return new CompetitionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CompetitionViewHolder competitionViewHolder, int i) {
        competitionViewHolder.competitionNameTextView.setText(competitions.get(i).getName());
    }

    @Override
    public int getItemCount() {
        return competitions.size();
    }


    public class CompetitionViewHolder extends RecyclerView.ViewHolder {
        private TextView competitionNameTextView;

        public CompetitionViewHolder(@NonNull View itemView) {
            super(itemView);
            competitionNameTextView = itemView.findViewById(R.id.text_view_competition_name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(competitions.get(position));
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Competition competition);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
