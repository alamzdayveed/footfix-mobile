package me.codegod.footfix.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import me.codegod.footfix.fragments.competitions.FixturesFragment;
import me.codegod.footfix.fragments.competitions.TableFragment;
import me.codegod.footfix.fragments.competitions.TeamsFragment;

public class CompetitionsViewPagerAdapter extends FragmentStatePagerAdapter {
    private static int TAB_COUNT = 3;

    public CompetitionsViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return TableFragment.newInstance();
            case 1:
                return FixturesFragment.newInstance();
            case 2:
                return TeamsFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return TAB_COUNT;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return TableFragment.TITLE;

            case 1:
                return FixturesFragment.TITLE;

            case 2:
                return TeamsFragment.TITLE;
        }
        return super.getPageTitle(position);
    }
}
