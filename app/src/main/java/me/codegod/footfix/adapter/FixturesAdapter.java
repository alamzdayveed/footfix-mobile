package me.codegod.footfix.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import me.codegod.footfix.R;
import me.codegod.footfix.Utils;
import me.codegod.footfix.model.fixtures.Fixture;

public class FixturesAdapter extends RecyclerView.Adapter<FixturesAdapter.FixtureViewHolder> {
    private List<Fixture> fixtures;
    private Context context;

    public FixturesAdapter(List<Fixture> fixtures, Context context) {
        this.fixtures = fixtures;
        this.context = context;
    }

    @NonNull
    @Override
    public FixtureViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.fixture_item, viewGroup, false);
        return new FixtureViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FixtureViewHolder fixtureViewHolder, int i) {
        Fixture fixture = fixtures.get(i);

        fixtureViewHolder.statusTextView.setText(fixture.getStatus());
        fixtureViewHolder.awayTeamScoreTextView.setText(String.valueOf(fixture.getAwayTeamScore()));
        fixtureViewHolder.awayTeamTextView.setText(fixture.getAwayTeamName());
        fixtureViewHolder.homeTeamScoreTextView.setText(String.valueOf(fixture.getHomeTeamScore()));
        fixtureViewHolder.homeTeamTextView.setText(fixture.getHomeTeamName());
        fixtureViewHolder.matchDayTextView.setText("MD: " + String.valueOf(fixture.getMatchday()));
        fixtureViewHolder.fixtureTimeTextView.setText(Utils.utcToDate(fixture.getUtcDate()));
    }

    @Override
    public int getItemCount() {
        return fixtures.size();
    }

    public static class FixtureViewHolder extends RecyclerView.ViewHolder {
        private TextView statusTextView,
            fixtureTimeTextView,
            matchDayTextView,
            homeTeamTextView,
            homeTeamScoreTextView,
            awayTeamTextView,
            awayTeamScoreTextView;

        public FixtureViewHolder(@NonNull View itemView) {
            super(itemView);

            statusTextView = (TextView) itemView.findViewById(R.id.status_text_view);
            fixtureTimeTextView = (TextView) itemView.findViewById(R.id.fixture_time_textview);
            matchDayTextView = (TextView) itemView.findViewById(R.id.match_day_textview);
            homeTeamTextView = (TextView) itemView.findViewById(R.id.home_team_textview);
            homeTeamScoreTextView = (TextView) itemView.findViewById(R.id.home_team_score_textview);
            awayTeamTextView = (TextView) itemView.findViewById(R.id.away_team_textview);
            awayTeamScoreTextView = (TextView) itemView.findViewById(R.id.away_team_score_textview);
        }
    }
}
