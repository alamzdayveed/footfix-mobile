package me.codegod.footfix.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import me.codegod.footfix.R;
import me.codegod.footfix.model.standings.Standing;

public class StandingsAdapter extends RecyclerView.Adapter<StandingsAdapter.StandingViewHolder> {
    private List<Standing> standings;
    private Context context;

    public StandingsAdapter(List<Standing> standings, Context context) {
        this.standings = standings;
        this.context = context;
    }

    @NonNull
    @Override
    public StandingViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.standing_item, viewGroup, false);

        return new StandingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StandingViewHolder standingViewHolder, int i) {
        Standing standing = standings.get(i);
        Glide.with(context).load(standing.getTeam().getCrestUrl()).into(standingViewHolder.teamEmblemImageView);

        standingViewHolder.teamPosititonTextView.setText(String.valueOf(standing.getPosition()));
        standingViewHolder.teamNameTextView.setText(standing.getTeam().getName());
        standingViewHolder.teamPointsTextView.setText(String.valueOf(standing.getPoints()));
        standingViewHolder.teamGoalDifferenceTextView.setText(String.valueOf(standing.getGoalDifference()));
        standingViewHolder.teamMatchPlayedTextView.setText(String.valueOf(standing.getPlayedGames()));
    }

    @Override
    public int getItemCount() {
        return standings.size();
    }

    public class StandingViewHolder extends RecyclerView.ViewHolder {
        private TextView teamPosititonTextView, teamNameTextView, teamMatchPlayedTextView, teamGoalDifferenceTextView, teamPointsTextView;
        private ImageView teamEmblemImageView;

        public StandingViewHolder(@NonNull View itemView) {
            super(itemView);

            teamPosititonTextView = (TextView) itemView.findViewById(R.id.team_position_text_view);
            teamNameTextView = (TextView) itemView.findViewById(R.id.team_name_text_view);
            teamGoalDifferenceTextView = (TextView) itemView.findViewById(R.id.team_goal_difference_text_view);
            teamPointsTextView = (TextView) itemView.findViewById(R.id.team_points_text_view);
            teamEmblemImageView = (ImageView) itemView.findViewById(R.id.team_emblem_image_view);
            teamMatchPlayedTextView = (TextView) itemView.findViewById(R.id.team_match_played_text_view);
        }
    }
}
