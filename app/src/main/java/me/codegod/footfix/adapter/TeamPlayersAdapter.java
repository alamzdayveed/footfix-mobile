package me.codegod.footfix.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import me.codegod.footfix.R;
import me.codegod.footfix.model.single_team.TeamPlayer;
import me.codegod.footfix.model.teams.Team;

public class TeamPlayersAdapter extends RecyclerView.Adapter<TeamPlayersAdapter.TeamPlayerViewHolder> {
    private List<TeamPlayer> teamPlayers;
    private Context context;

    public TeamPlayersAdapter(List<TeamPlayer> teamPlayers, Context context) {
        this.teamPlayers = teamPlayers;
        this.context = context;
    }

    @NonNull
    @Override
    public TeamPlayerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.team_players_item, viewGroup, false);

        return new TeamPlayerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TeamPlayerViewHolder teamPlayerViewHolder, int i) {
        teamPlayerViewHolder.indexTextView.setText(String.valueOf(i + 1));
        teamPlayerViewHolder.playerNameTextView.setText(teamPlayers.get(i).getName());
        teamPlayerViewHolder.playerPositionTextView.setText(teamPlayers.get(i).getPosition());
    }

    @Override
    public int getItemCount() {
        return teamPlayers.size();
    }

    public class TeamPlayerViewHolder extends RecyclerView.ViewHolder {
        private TextView indexTextView, playerNameTextView, playerPositionTextView;

        public TeamPlayerViewHolder(@NonNull View itemView) {
            super(itemView);
            indexTextView = itemView.findViewById(R.id.index);
            playerNameTextView = itemView.findViewById(R.id.player_name_text_view);
            playerPositionTextView = itemView.findViewById(R.id.player_position_text_view);
        }
    }
}
