package me.codegod.footfix.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import me.codegod.footfix.R;
import me.codegod.footfix.model.teams.Team;

public class TeamsAdapter extends RecyclerView.Adapter<TeamsAdapter.TeamViewHolder> {
    private List<Team> teams;
    private Context context;

    private OnItemClickListener listener;

    public TeamsAdapter(List<Team> teams, Context context) {
        this.teams = teams;
        this.context = context;
    }

    @NonNull
    @Override
    public TeamViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.team_item, viewGroup, false);

        return new TeamViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TeamViewHolder teamViewHolder, int i) {
        Glide.with(context).load(teams.get(i).getCrestUrl()).into(teamViewHolder.emblemImageView);

        teamViewHolder.teamNameTextView.setText(teams.get(i).getName());
    }

    @Override
    public int getItemCount() {
        return teams.size();
    }

    public class TeamViewHolder extends RecyclerView.ViewHolder {
        private ImageView emblemImageView;
        private TextView teamNameTextView;

        public TeamViewHolder(@NonNull View itemView) {
            super(itemView);

            emblemImageView = (ImageView) itemView.findViewById(R.id.emblem_image_view);
            teamNameTextView = (TextView) itemView.findViewById(R.id.team_name_text_view);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(teams.get(position));
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Team team);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
