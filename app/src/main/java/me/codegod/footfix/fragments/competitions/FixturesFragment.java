package me.codegod.footfix.fragments.competitions;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.List;

import me.codegod.footfix.R;
import me.codegod.footfix.adapter.FixturesAdapter;
import me.codegod.footfix.fragments.main.CompetitionsFragment;
import me.codegod.footfix.model.fixtures.Fixture;
import me.codegod.footfix.model.fixtures.FixtureResponse;
import me.codegod.footfix.rest.ApiClient;
import me.codegod.footfix.rest.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FixturesFragment extends Fragment {
    public static final String TITLE = "Fixtures";
    public static final String TAG = FixturesFragment.class.getSimpleName();

    RecyclerView fixturesRecyclerView;
    SharedPreferences preferences;

    public static FixturesFragment newInstance() {
        return new FixturesFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_competition_fixtures, container, false);

        fixturesRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        fixturesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<FixtureResponse> call = apiService.getCompetitionFixtures(String.valueOf(preferences.getInt(CompetitionsFragment.COMPETITION_ID, 0)));
        call.enqueue(new Callback<FixtureResponse>() {
            @Override
            public void onResponse(Call<FixtureResponse> call, Response<FixtureResponse> response) {
                List<Fixture> fixtures = response.body().getFixtures();

                Collections.reverse(fixtures);

                fixturesRecyclerView.setAdapter(new FixturesAdapter(fixtures, getActivity()));
                fixturesRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
            }

            @Override
            public void onFailure(Call<FixtureResponse> call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }
}
