package me.codegod.footfix.fragments.competitions;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import me.codegod.footfix.R;
import me.codegod.footfix.adapter.StandingsAdapter;
import me.codegod.footfix.adapter.TeamsAdapter;
import me.codegod.footfix.fragments.main.CompetitionsFragment;
import me.codegod.footfix.model.standings.Standing;
import me.codegod.footfix.model.standings.StandingResponse;
import me.codegod.footfix.model.teams.Team;
import me.codegod.footfix.model.teams.TeamResponse;
import me.codegod.footfix.rest.ApiClient;
import me.codegod.footfix.rest.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TableFragment extends Fragment {
    public static final String TITLE = "Table";
    public static final String TAG = TableFragment.class.getSimpleName();

    RecyclerView tableRecyclerView;
    SharedPreferences preferences;

    public static TableFragment newInstance() {
        return new TableFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_competition_table, container, false);

        tableRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        tableRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<StandingResponse> call = apiService.getCompetitionStanding(String.valueOf(preferences.getInt(CompetitionsFragment.COMPETITION_ID, 0)));
        call.enqueue(new Callback<StandingResponse>() {
            @Override
            public void onResponse(Call<StandingResponse> call, Response<StandingResponse> response) {
                List<Standing> standings = response.body().getTableStandings();
//                Log.e(TAG, String.valueOf(standings.size()));
                final StandingsAdapter adapter = new StandingsAdapter(standings, getActivity());

                tableRecyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<StandingResponse> call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }
}
