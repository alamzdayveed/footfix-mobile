package me.codegod.footfix.fragments.competitions;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;

import me.codegod.footfix.R;
import me.codegod.footfix.adapter.TeamPlayersAdapter;
import me.codegod.footfix.model.single_team.SingleTeamResponse;
import me.codegod.footfix.model.single_team.TeamPlayer;
import me.codegod.footfix.rest.ApiClient;
import me.codegod.footfix.rest.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TeamBottomSheetFragment extends BottomSheetDialogFragment {
    Button closeBtn;
    TextView teamNameTextView;
    ImageView emblemImageView;
    RecyclerView teamPlayersRecyclerView;

    SharedPreferences preferences;

    TeamPlayersAdapter adapter;

    public TeamBottomSheetFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_team_bottom_sheet_dialog, container, false);

        closeBtn = (Button) view.findViewById(R.id.close_button);
        teamNameTextView = (TextView) view.findViewById(R.id.team_name_text_view);
        emblemImageView = (ImageView) view.findViewById(R.id.emblem_image_view);
        teamPlayersRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        teamPlayersRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        teamPlayersRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<SingleTeamResponse> call = apiService.getTeam(String.valueOf(preferences.getInt(TeamsFragment.TEAM_ID, 0)));
        call.enqueue(new Callback<SingleTeamResponse>() {
            @Override
            public void onResponse(Call<SingleTeamResponse> call, Response<SingleTeamResponse> response) {
                teamNameTextView.setText(response.body().getName());
                Picasso.get().load(response.body().getCrestUrl()).into(emblemImageView);

                List<TeamPlayer> players = response.body().getPlayers();

                adapter = new TeamPlayersAdapter(players, getActivity());

                teamPlayersRecyclerView.setAdapter(adapter);
                teamPlayersRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
            }

            @Override
            public void onFailure(Call<SingleTeamResponse> call, Throwable t) {

            }
        });
    }
}
