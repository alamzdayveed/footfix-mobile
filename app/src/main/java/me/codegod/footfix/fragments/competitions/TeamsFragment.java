package me.codegod.footfix.fragments.competitions;

import android.arch.lifecycle.ViewModelProviders;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import me.codegod.footfix.R;
import me.codegod.footfix.adapter.TeamsAdapter;
import me.codegod.footfix.model.teams.Team;
import me.codegod.footfix.room.viewmodel.TeamViewModel;

public class TeamsFragment extends Fragment {
    public static final String TAG = TeamsFragment.class.getSimpleName();
    public static final String TITLE = "Teams";
    public static final String TEAM_ID = "TEAM_ID";
    public static final String TEAM_NAME = "TEAM_NAME";

    RecyclerView teamsRecyclerView;
    TeamsAdapter adapter;
    TeamViewModel teamViewModel;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    public static TeamsFragment newInstance() {
        return new TeamsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_competition_teams, container, false);

        teamsRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        teamsRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        teamViewModel = ViewModelProviders.of(this).get(TeamViewModel.class);
        List<Team> teams = teamViewModel.getAllTeams();

        adapter = new TeamsAdapter(teams, getActivity());

        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        editor = preferences.edit();

        adapter.setOnItemClickListener(new TeamsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Team team) {
                showBottomSheetDialogFragment();
                editor.putInt(TEAM_ID, team.getId());
                editor.apply();
            }
        });

        teamsRecyclerView.setAdapter(adapter);
    }

    public void showBottomSheetDialogFragment() {
        TeamBottomSheetFragment bottomSheetFragment = new TeamBottomSheetFragment();
        bottomSheetFragment.show(getFragmentManager(), bottomSheetFragment.getTag());
    }
}
