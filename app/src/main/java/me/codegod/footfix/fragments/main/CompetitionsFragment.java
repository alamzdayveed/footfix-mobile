package me.codegod.footfix.fragments.main;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import me.codegod.footfix.CompetitionActivity;
import me.codegod.footfix.R;
import me.codegod.footfix.adapter.CompetitionsAdapter;
import me.codegod.footfix.model.competitions.Competition;
import me.codegod.footfix.room.viewmodel.CompetitionViewModel;

public class CompetitionsFragment extends Fragment {
    public static final String TAG = "hello-there";
    public static final String COMPETITION_NAME = "COMPETITION_NAME";
    public static final String COMPETITION_ID = "COMPETITION_ID";

    RecyclerView competitionsRecyclerView;
    public CompetitionsAdapter adapter;
    private CompetitionViewModel competitionViewModel;

    public static CompetitionsFragment newInstance() {
        return new CompetitionsFragment();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        competitionViewModel = ViewModelProviders.of(this).get(CompetitionViewModel.class);
        List<Competition> competitions = competitionViewModel.getAllCompetitions();

        adapter = new CompetitionsAdapter(competitions, getActivity());

        adapter.setOnItemClickListener(new CompetitionsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Competition competition) {
                Intent intent = new Intent(getActivity(), CompetitionActivity.class);
                intent.putExtra(COMPETITION_NAME, competition.getName());
                intent.putExtra(COMPETITION_ID, competition.getId());
                startActivity(intent);
            }
        });

        competitionsRecyclerView.setAdapter(adapter);
        competitionsRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_competitions, container, false);

        competitionsRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        competitionsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return view;
    }
}
