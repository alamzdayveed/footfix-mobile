package me.codegod.footfix.model.competitions;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CompetitionResponse {
    @SerializedName("count")
    private int count;

    @SerializedName("filters")
    private Filters filters;

    @SerializedName("competitions")
    private List<Competition> competitions;

    public Filters getFilters() {
        return filters;
    }

    public void setFilters(Filters filters) {
        this.filters = filters;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Competition> getCompetitions() {
        return competitions;
    }

    public void setCompetitions(List<Competition> competitions) {
        this.competitions = competitions;
    }

    private class Filters {
        public Filters() {
        }
    }
}
