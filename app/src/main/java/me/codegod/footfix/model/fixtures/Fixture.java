package me.codegod.footfix.model.fixtures;

import com.google.gson.annotations.SerializedName;

public class Fixture {
    @SerializedName("id")
    private int id;

    @SerializedName("utcDate")
    private String utcDate;

    @SerializedName("status")
    private String status;

    @SerializedName("matchday")
    private int matchday;

    @SerializedName("score")
    private Score score;

    @SerializedName("homeTeam")
    private Team homeTeam;

    @SerializedName("awayTeam")
    private Team awayTeam;

    public Fixture(int id, String utcDate, String status, int matchday, Score score, Team homeTeam, Team awayTeam) {
        this.id = id;
        this.utcDate = utcDate;
        this.status = status;
        this.matchday = matchday;
        this.score = score;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUtcDate() {
        return utcDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getMatchday() {
        return matchday;
    }

    public int getHomeTeamScore() {
        return score.getFullTime().homeTeam < 0 ? 0 : score.getFullTime().homeTeam;
    }

    public int getAwayTeamScore() {
        return score.getFullTime().awayTeam < 0 ? 0 : score.getFullTime().awayTeam;
    }

    public String getHomeTeamName() {
        return homeTeam.getName();
    }

    public String getAwayTeamName() {
        return awayTeam.getName();
    }

    public void setHomeTeam(Team homeTeam) {
        this.homeTeam = homeTeam;
    }

    public Team getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(Team awayTeam) {
        this.awayTeam = awayTeam;
    }

    private class Score {
        @SerializedName("fullTime")
        private FullTime fullTime;

        public Score(FullTime fullTime) {
            this.fullTime = fullTime;
        }

        public FullTime getFullTime() {
            return fullTime;
        }

        public void setFullTime(FullTime fullTime) {
            this.fullTime = fullTime;
        }

        private class FullTime {
            @SerializedName("homeTeam")
            private int homeTeam;

            @SerializedName("awayTeam")
            private int awayTeam;

            public FullTime(int homeTeam, int awayTeam) {
                this.homeTeam = homeTeam;
                this.awayTeam = awayTeam;
            }

            public int getHomeTeam() {
                return homeTeam;
            }

            public void setHomeTeam(int homeTeam) {
                this.homeTeam = homeTeam;
            }

            public int getAwayTeam() {
                return awayTeam;
            }

            public void setAwayTeam(int awayTeam) {
                this.awayTeam = awayTeam;
            }
        }
    }

    private class Team {
        @SerializedName("id")
        private int id;

        @SerializedName("name")
        private String name;

        public Team(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
