package me.codegod.footfix.model.fixtures;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FixtureResponse {
    @SerializedName("matches")
    private List<Fixture> fixtures;

    public FixtureResponse(List<Fixture> fixtures) {
        this.fixtures = fixtures;
    }

    public List<Fixture> getFixtures() {
        return fixtures;
    }

    public void setFixtures(List<Fixture> fixtures) {
        this.fixtures = fixtures;
    }
}
