package me.codegod.footfix.model.single_team;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SingleTeamResponse {
    @SerializedName("name")
    private String name;

    @SerializedName("crestUrl")
    private String crestUrl;

    @SerializedName("squad")
    private List<TeamPlayer> players;

    public SingleTeamResponse(String name, String crestUrl, List<TeamPlayer> players) {
        this.name = name;
        this.crestUrl = crestUrl;
        this.players = players;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCrestUrl() {
        return crestUrl;
    }

    public void setCrestUrl(String crestUrl) {
        this.crestUrl = crestUrl;
    }

    public List<TeamPlayer> getPlayers() {
        return players;
    }

    public void setPlayers(List<TeamPlayer> players) {
        this.players = players;
    }
}
