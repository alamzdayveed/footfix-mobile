package me.codegod.footfix.model.standings;

import com.google.gson.annotations.SerializedName;

public class Standing {
    @SerializedName("position")
    private int position;

    @SerializedName("team")
    private Team team;

    @SerializedName("playedGames")
    private int playedGames;

    @SerializedName("points")
    private int points;

    @SerializedName("goalDifference")
    private int goalDifference;

    public Standing(int position, Team team, int playedGames, int points, int goalDifference) {
        this.position = position;
        this.team = team;
        this.playedGames = playedGames;
        this.points = points;
        this.goalDifference = goalDifference;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public int getPlayedGames() {
        return playedGames;
    }

    public void setPlayedGames(int playedGames) {
        this.playedGames = playedGames;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getGoalDifference() {
        return goalDifference;
    }

    public void setGoalDifference(int goalDifference) {
        this.goalDifference = goalDifference;
    }

    public class Team {
        @SerializedName("id")
        private int id;

        @SerializedName("name")
        private String name;

        @SerializedName("crestUrl")
        private String crestUrl;

        public Team(int id, String name, String crestUrl) {
            this.id = id;
            this.name = name;
            this.crestUrl = crestUrl;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCrestUrl() {
            return crestUrl;
        }

        public void setCrestUrl(String crestUrl) {
            this.crestUrl = crestUrl;
        }
    }
}
