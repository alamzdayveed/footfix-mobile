package me.codegod.footfix.model.standings;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StandingResponse {
    private List<StandingListItem> standings;

    public StandingResponse(List<StandingListItem> standings) {
        this.standings = standings;
    }

    public List<StandingListItem> getStandings() {
        return standings;
    }

    public void setStandings(List<StandingListItem> standings) {
        this.standings = standings;
    }

    public List<Standing> getTableStandings() {
        return this.standings.get(0).getStandings();
    }

    private class StandingListItem {
        @SerializedName("table")
        private List<Standing> standings;

        public StandingListItem(List<Standing> standings) {
            this.standings = standings;
        }

        public List<Standing> getStandings() {
            return standings;
        }

        public void setStandings(List<Standing> standings) {
            this.standings = standings;
        }
    }
}
