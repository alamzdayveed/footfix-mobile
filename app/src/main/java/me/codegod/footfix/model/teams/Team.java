package me.codegod.footfix.model.teams;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "team_table")
public class Team {
    @PrimaryKey
    @SerializedName("id")
    private int id;

    private int competitionId;

    @SerializedName("name")
    private String name;

    @SerializedName("crestUrl")
    private String crestUrl;

    public Team(int id, String name, String crestUrl) {
        this.id = id;
        this.name = name;
        this.crestUrl = crestUrl;
    }

    public int getCompetitionId() {
        return competitionId;
    }

    public void setCompetitionId(int competitionId) {
        this.competitionId = competitionId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCrestUrl() {
        return crestUrl;
    }

    public void setCrestUrl(String crestUrl) {
        this.crestUrl = crestUrl;
    }
}
