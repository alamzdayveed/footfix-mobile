package me.codegod.footfix.rest;

import me.codegod.footfix.model.competitions.CompetitionResponse;
import me.codegod.footfix.model.fixtures.FixtureResponse;
import me.codegod.footfix.model.single_team.SingleTeamResponse;
import me.codegod.footfix.model.standings.StandingResponse;
import me.codegod.footfix.model.teams.TeamResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface ApiInterface {
    @Headers("X-Auth-Token: 4dc80b1ffc2044aab34151af08846d84")
    @GET("competitions")
    Call<CompetitionResponse> getAllCompetitions();

    @Headers("X-Auth-Token: 4dc80b1ffc2044aab34151af08846d84")
    @GET("matches")
    Call<FixtureResponse> getAllFixtures();

    @Headers("X-Auth-Token: 4dc80b1ffc2044aab34151af08846d84")
    @GET("competitions/{competitionId}/teams")
    Call<TeamResponse> getAllCompetitionTeams(@Path("competitionId") String competitionId);

    @Headers("X-Auth-Token: 4dc80b1ffc2044aab34151af08846d84")
    @GET("teams/{teamId}")
    Call<SingleTeamResponse> getTeam(@Path("teamId") String teamId);

    @Headers("X-Auth-Token: 4dc80b1ffc2044aab34151af08846d84")
    @GET("competitions/{competitionId}/standings")
    Call<StandingResponse> getCompetitionStanding(@Path("competitionId") String competitionId);

    @Headers("X-Auth-Token: 4dc80b1ffc2044aab34151af08846d84")
    @GET("competitions/{competitionId}/matches")
    Call<FixtureResponse> getCompetitionFixtures(@Path("competitionId") String competitionId);
}
