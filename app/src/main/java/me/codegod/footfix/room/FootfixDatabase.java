package me.codegod.footfix.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import me.codegod.footfix.model.competitions.Competition;
import me.codegod.footfix.model.teams.Team;
import me.codegod.footfix.room.dao.CompetitionDao;
import me.codegod.footfix.room.dao.TeamDao;

@Database(entities = {Competition.class, Team.class}, version = 2)
public abstract class FootfixDatabase extends RoomDatabase {
    private static FootfixDatabase instance;

    public abstract CompetitionDao competitionDao();
    public abstract TeamDao teamDao();

    public static synchronized FootfixDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(
                context.getApplicationContext(),
                FootfixDatabase.class,
                "footfix_database"
            )
            .fallbackToDestructiveMigration()
            .build();
        }

        return instance;
    }
}
