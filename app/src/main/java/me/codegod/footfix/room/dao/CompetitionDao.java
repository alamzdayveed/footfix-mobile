package me.codegod.footfix.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import me.codegod.footfix.model.competitions.Competition;

@Dao
public interface CompetitionDao {
    @Insert
    void insertAll(List<Competition> competitions);

    @Query("DELETE from competition_table")
    void deleteAll();

    @Query("SELECT * FROM competition_table")
    List<Competition> getAllCompetitions();
}
