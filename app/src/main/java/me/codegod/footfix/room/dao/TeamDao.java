package me.codegod.footfix.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import me.codegod.footfix.model.teams.Team;

@Dao
public interface TeamDao {
    @Insert
    void insertAll(List<Team> teams);

    @Query("DELETE from team_table WHERE competitionId = :competitionId")
    void deleteAll(String competitionId);

    @Query("SELECT * FROM team_table WHERE competitionId = :competitionId")
    List<Team> getCompetitionTeams(String competitionId);
}
