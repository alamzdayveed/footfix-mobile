package me.codegod.footfix.room.repository;

import android.app.Application;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import me.codegod.footfix.Utils;
import me.codegod.footfix.model.competitions.Competition;
import me.codegod.footfix.model.competitions.CompetitionResponse;
import me.codegod.footfix.rest.ApiClient;
import me.codegod.footfix.rest.ApiInterface;
import me.codegod.footfix.room.FootfixDatabase;
import me.codegod.footfix.room.dao.CompetitionDao;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompetitionRepository {
    private CompetitionDao competitionDao;
    private Application application;

    public CompetitionRepository(Application application) {
        this.application = application;
        FootfixDatabase database = FootfixDatabase.getInstance(application);
        competitionDao = database.competitionDao();
    }

    public void insertAll(List<Competition> competitions) {
        new InsertAllCompetitionsAsyncTask(competitionDao).execute(competitions.toArray(new Competition[0]));
    }

    public void deleteAll() {
        new DeleteAllCompetitionsAsyncTask(competitionDao).execute();
    }

    public List<Competition> getAllCompetitions() {
        List<Competition> competitions = new ArrayList<>();

        try {
            competitions = new GetAllCompetitionsAsyncTask(competitionDao, application).execute().get();
        } catch (Exception ie) {

        }

        return competitions;
    }

    private static class InsertAllCompetitionsAsyncTask extends AsyncTask<Competition, Void, Void> {
        private CompetitionDao competitionDao;

        public InsertAllCompetitionsAsyncTask(CompetitionDao competitionDao) {
            this.competitionDao = competitionDao;
        }

        @Override
        protected Void doInBackground(Competition... competitions) {
            List<Competition> competitionsList = new ArrayList<>(Arrays.asList(competitions));

            competitionDao.insertAll(competitionsList);
            return null;
        }
    }

    private static class DeleteAllCompetitionsAsyncTask extends AsyncTask<Void, Void, Void> {
        private CompetitionDao competitionDao;

        private DeleteAllCompetitionsAsyncTask(CompetitionDao competitionDao) {
            this.competitionDao = competitionDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            competitionDao.deleteAll();
            return null;
        }
    }

    private static class GetAllCompetitionsAsyncTask extends AsyncTask<Void, Void, List<Competition>> {
        private CompetitionDao competitionDao;
        Application application;

        private GetAllCompetitionsAsyncTask(CompetitionDao competitionDao, Application application) {
            this.competitionDao = competitionDao;
            this.application = application;
        }

        @Override
        protected List<Competition> doInBackground(Void... voids) {
            if (Utils.isNetworkAvailable(application)) {
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                Call<CompetitionResponse> call = apiService.getAllCompetitions();
                call.enqueue(new Callback<CompetitionResponse>() {
                    @Override
                    public void onResponse(Call<CompetitionResponse> call, Response<CompetitionResponse> response) {
                        List<Competition> competitions = new ArrayList<>();
                        for (Competition competition : response.body().getCompetitions()) {
                            if (competition.getPlan().equals("TIER_ONE")) {
                                competitions.add(competition);
                            }
                        }

                        new DeleteAllCompetitionsAsyncTask(competitionDao).execute();
                        new InsertAllCompetitionsAsyncTask(competitionDao).execute(competitions.toArray(new Competition[0]));
                    }

                    @Override
                    public void onFailure(Call<CompetitionResponse> call, Throwable t) {
                    }
                });
            } else {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(application.getApplicationContext(), "No internet. Please try again later.", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            return competitionDao.getAllCompetitions();
        }
    }
}
