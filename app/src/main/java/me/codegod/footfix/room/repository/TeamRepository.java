package me.codegod.footfix.room.repository;

import android.app.Application;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import me.codegod.footfix.Utils;
import me.codegod.footfix.fragments.main.CompetitionsFragment;
import me.codegod.footfix.model.teams.Team;
import me.codegod.footfix.model.teams.TeamResponse;
import me.codegod.footfix.rest.ApiClient;
import me.codegod.footfix.rest.ApiInterface;
import me.codegod.footfix.room.FootfixDatabase;
import me.codegod.footfix.room.dao.TeamDao;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TeamRepository {
    private TeamDao teamDao;
    private Application application;

    public TeamRepository(Application application) {
        this.application = application;
        FootfixDatabase database = FootfixDatabase.getInstance(application);
        teamDao = database.teamDao();
    }

    public void insertAll(List<Team> teams) {
        new InsertAllCompetitionTeamsAsyncTask(teamDao).execute(teams.toArray(new Team[0]));
    }

    public void deleteAll() {
        new DeleteAllCompetitionTeamsAsyncTask(teamDao, application).execute();
    }

    public List<Team> getAllCompetitionTeams() {
        List<Team> teams = new ArrayList<>();

        try {
            teams = new GetAllCompetitionTeamsAsyncTask(teamDao, application).execute().get();
        } catch (Exception ie) {

        }

        return teams;
    }

    private static class InsertAllCompetitionTeamsAsyncTask extends AsyncTask<Team, Void, Void> {
        private TeamDao teamDao;

        public InsertAllCompetitionTeamsAsyncTask(TeamDao teamDao) {
            this.teamDao = teamDao;
        }

        @Override
        protected Void doInBackground(Team... teams) {
            List<Team> teamList = new ArrayList<>(Arrays.asList(teams));

            teamDao.insertAll(teamList);
            return null;
        }
    }

    private static class DeleteAllCompetitionTeamsAsyncTask extends AsyncTask<Void, Void, Void> {
        private TeamDao teamDao;
        Application application;
        SharedPreferences preferences;
        String competitionId;

        private DeleteAllCompetitionTeamsAsyncTask(TeamDao teamDao, Application application) {
            this.teamDao = teamDao;
            this.application = application;
            preferences = PreferenceManager.getDefaultSharedPreferences(application.getApplicationContext());
            competitionId = String.valueOf(preferences.getInt(CompetitionsFragment.COMPETITION_ID, 0));
        }

        @Override
        protected Void doInBackground(Void... voids) {
            teamDao.deleteAll(competitionId);
            return null;
        }
    }

    private static class GetAllCompetitionTeamsAsyncTask extends AsyncTask<Void, Void, List<Team>> {
        private TeamDao teamDao;
        Application application;
        SharedPreferences preferences;
        String competitionId;

        private GetAllCompetitionTeamsAsyncTask(TeamDao teamDao, Application application) {
            this.teamDao = teamDao;
            this.application = application;
            preferences = PreferenceManager.getDefaultSharedPreferences(application.getApplicationContext());
            competitionId = String.valueOf(preferences.getInt(CompetitionsFragment.COMPETITION_ID, 0));
        }

        @Override
        protected List<Team> doInBackground(Void... voids) {
            if (Utils.isNetworkAvailable(application)) {
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                Call<TeamResponse> call = apiService.getAllCompetitionTeams(String.valueOf(preferences.getInt(CompetitionsFragment.COMPETITION_ID, 0)));
                call.enqueue(new Callback<TeamResponse>() {
                    @Override
                    public void onResponse(Call<TeamResponse> call, Response<TeamResponse> response) {
                        List<Team> teams = response.body().getTeams();
                        for (Team team : response.body().getTeams()) {
                            team.setCompetitionId(Integer.valueOf(competitionId));
                        }

                        new DeleteAllCompetitionTeamsAsyncTask(teamDao, application).execute();
                        new InsertAllCompetitionTeamsAsyncTask(teamDao).execute(teams.toArray(new Team[0]));
                    }

                    @Override
                    public void onFailure(Call<TeamResponse> call, Throwable t) {
                    }
                });
            } else {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(application.getApplicationContext(), "No internet. Please try again later.", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            return teamDao.getCompetitionTeams(competitionId);
        }
    }

}
