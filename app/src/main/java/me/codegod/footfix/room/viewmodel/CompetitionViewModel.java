package me.codegod.footfix.room.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

import me.codegod.footfix.model.competitions.Competition;
import me.codegod.footfix.room.repository.CompetitionRepository;

public class CompetitionViewModel extends AndroidViewModel {
    private CompetitionRepository repository;

    public CompetitionViewModel(@NonNull Application application) {
        super(application);

        repository = new CompetitionRepository(application);
    }

    public void insertAll(List<Competition> competitions) {
        repository.insertAll(competitions);
    }

    public void deleteAll() {
        repository.deleteAll();
    }

    public List<Competition> getAllCompetitions() {
        return repository.getAllCompetitions();
    }
}
