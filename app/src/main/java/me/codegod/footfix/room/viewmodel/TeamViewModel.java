package me.codegod.footfix.room.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

import java.util.List;

import me.codegod.footfix.model.teams.Team;
import me.codegod.footfix.room.repository.TeamRepository;

public class TeamViewModel extends AndroidViewModel {
    private TeamRepository repository;

    public TeamViewModel(@NonNull Application application) {
        super(application);

        repository = new TeamRepository(application);
    }

    public List<Team> getAllTeams() {
        return repository.getAllCompetitionTeams();
    }
}
